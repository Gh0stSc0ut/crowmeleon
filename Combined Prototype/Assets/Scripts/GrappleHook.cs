﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHook : MonoBehaviour
{

    DistanceJoint2D joint;
    Vector3 targetPos;
    RaycastHit2D hit;
    public float distance = 10f;
    public LayerMask mask;
    public LineRenderer grapple;
    public float jumpspeed = 5f;
    public Rigidbody2D playerRigidbody;
    public Animator anim;

    public float grappleSpeed = 0.2f;
    bool isGrapple = false;
    float lickDamage = 50f;

    public GameObject testPoint;
    public LevelController levelController;

    public AudioSource lick;

    bool isSwing = false;
    float swingDis = 0f;

    // Use this for initialization
    void Start()
    {
        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;
        grapple.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Q))
        {
            targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0;

            hit = Physics2D.Raycast(transform.position, targetPos - transform.position, distance, mask);

            if (hit.collider != null)
            {
                joint.enabled = true;

                //joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>();

                float connectX = hit.collider.transform.position.x - hit.point.x;
                float connectY = hit.collider.transform.position.y + hit.point.y;
                //joint.connectedAnchor = new Vector2(connectX, connectY);
                joint.distance = Vector2.Distance(transform.position, hit.point);

                Vector3 startPoint = new Vector3(transform.position.x, transform.position.y, 0f);
                Vector3 endPoint = new Vector3(hit.transform.position.x, hit.transform.position.y, -5f);
                testPoint.transform.position = hit.point;

                grapple.enabled = true;
                grapple.SetPosition(0, startPoint);
                grapple.SetPosition(1, endPoint);

                if (hit.collider.gameObject.tag == "Enemy")
                {
                    EnemyHealth_Level hurt = hit.collider.GetComponent<EnemyHealth_Level>();
                    hurt.Damage(lickDamage);
                    isGrapple = false;
                }
                else if (hit.collider.gameObject.tag == "Food")
                {
                    FoodScript food = hit.collider.GetComponent<FoodScript>();
                    levelController.eaten += 1;
                    food.Damage(lickDamage);
                    isGrapple = false;
                }
            }
            else return;
        }

        /*if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Q))
        {
            targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0;

            hit = Physics2D.Raycast(transform.position, targetPos - transform.position, distance, mask);

            if (hit.collider != null)
            {
                joint.enabled = true;

                joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>();

                float connectX = hit.collider.transform.position.x - hit.point.x;
                float connectY = hit.collider.transform.position.y + hit.point.y;
                joint.connectedAnchor = new Vector2(connectX, connectY);
                joint.distance = Vector2.Distance(transform.position, hit.point);

                testPoint.transform.position = hit.point;

                grapple.enabled = true;
                grapple.SetPosition(0, transform.position);
                grapple.SetPosition(1, hit.point);
            }
        }*/

        if (Input.GetKey(KeyCode.E))
        {
            grapple.SetPosition(0, transform.position);
            isGrapple = true;
            Retract();
            anim.SetBool("Grapple", true);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            grapple.SetPosition(0, transform.position);
            isSwing = true;
            anim.SetBool("Grapple", true);
        }

        if (Input.GetKeyDown(KeyCode.Q)) {
            swingDis = joint.distance;
            Swing();
        }

        if (Input.GetKeyUp(KeyCode.E) || Input.GetKeyUp(KeyCode.Q))
        {
            joint.enabled = false;
            isGrapple = false;
            grapple.enabled = false;
            anim.SetBool("Grapple", false);
            if (hit.collider != null && hit.collider.gameObject.tag != "Enemy" && hit.collider.gameObject.tag != "Food") {
                playerRigidbody.AddForce(Vector2.up * jumpspeed, ForceMode2D.Impulse);
            } 
        }
        if (isGrapple == true)
        {
            lick.UnPause();
        }
        else
        {
            lick.Pause();
        }

    }

    void Swing() {
        joint.distance = swingDis;
    }

    void Retract()
    {
        if (isGrapple == true)
        {
            if (joint.distance < 0)
            {
                joint.distance = 0;
            }
            joint.distance -= grappleSpeed;
        }
    }
}
