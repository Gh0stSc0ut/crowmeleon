﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {

    public float damage;
    public float speed;
    public float pushBackForce = 20f;
    EnemyHealth_Level enemyHealth;

	// Use this for initialization
	void Start () {
        enemyHealth = GetComponent<EnemyHealth_Level>();
        damage = enemyHealth.enemeyAttack;
        speed = enemyHealth.enemeyAttackSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            PlayerHealth playerHealth = collision.gameObject.GetComponent<PlayerHealth>();
            playerHealth.Damage(damage);

            pushBack(collision.transform);
        }
    }

    void pushBack(Transform pushedObject) {
        Vector2 pushDirection = new Vector2(0, pushedObject.position.y - transform.position.y).normalized;
        pushDirection *= pushBackForce;
        Rigidbody2D rigidbody = pushedObject.gameObject.GetComponent<Rigidbody2D>();
        rigidbody.velocity = Vector2.zero;
        rigidbody.AddForce(pushDirection, ForceMode2D.Impulse);
    }
}
