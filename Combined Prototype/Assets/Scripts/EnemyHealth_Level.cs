﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth_Level : MonoBehaviour {

	float enemeyHealth = 100f;
	public float enemeyAttack = 500f;
	public float enemeyAttackSpeed = 2f;
	public float enemeySpeed = 2f;
    float currentHealth;
    public bool isHit = false;

	LevelController levelCon;

	// Use this for initialization
	void Start () {
        levelCon = GameObject.FindWithTag("LevelCon").GetComponent<LevelController>();
        currentHealth = enemeyHealth;
	}
	
	// Update is called once per frame
	void Update () {
        if (levelCon.eaten % 5 == 0)
        {
            //Might level up enemies continuously while eaten is a multiple of 5
            LevelUp();
        }
        else return;
	}

	void LevelUp () {
		enemeyHealth += 50f;
		enemeyAttack += 5f;
		enemeyAttackSpeed += 1f;
		enemeySpeed += 2f;
	}

    public void Damage(float damage) {
        currentHealth -= damage;
        if (currentHealth <= 0) {
            Death();
        }
        isHit = false;
    }

    void Death() {
        Destroy(gameObject);
    }
}
