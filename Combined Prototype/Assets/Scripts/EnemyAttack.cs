﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public Animator anim;

    public AudioSource screech;
    public AudioSource flap;

	// Use this for initialization
	void Start () {
        screech.Pause();
	}
	
	// Update is called once per frame
	void Update () {
        flap.UnPause();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        anim.SetBool("Attack", true);
        screech.Play();
        screech.UnPause();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        anim.SetBool("Attack", false);
        screech.Pause();
    }
}
