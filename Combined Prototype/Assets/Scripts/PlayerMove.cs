﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	Rigidbody2D playerRigidbody;
	public float speed = 3f;
	public float jumpspeed = 5f;
	private bool isGrounded;
	float targetmovespeed;
	public LayerMask groundLayers;
	private bool facingLeft;
    Animator anim;
    public AudioSource footsteps;
    public AudioSource jump;

    bool isMoving = false;

	void Awake () {
		playerRigidbody = GetComponent <Rigidbody2D> ();
        anim = GetComponent<Animator>();
        footsteps = footsteps.GetComponent<AudioSource>();
        jump = jump.GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

		isGrounded = Physics2D.OverlapArea(new Vector2(transform.position.x - 2f, transform.position.y - 2f),
			new Vector2(transform.position.x + 2f, transform.position.y - 2.01f), groundLayers);

		targetmovespeed = Mathf.Lerp (playerRigidbody.velocity.x, Input.GetAxisRaw ("Horizontal") * speed * Time.deltaTime, Time.deltaTime * 10);
		playerRigidbody.velocity = new Vector2 (targetmovespeed, playerRigidbody.velocity.y);

		if (Input.GetKey(KeyCode.A)) {
			transform.position += Vector3.left * speed * Time.deltaTime;
			facingLeft = true;
			Facing ();
            isMoving = true;
            anim.SetBool("isWalking", true);
		}
		if (Input.GetKey(KeyCode.D)) {
			transform.position += Vector3.right * speed * Time.deltaTime;
			facingLeft = false;
			Facing ();
            isMoving = true;
            anim.SetBool("isWalking", true);
        }

		if (Input.GetKeyDown (KeyCode.W) && isGrounded == true) {
			playerRigidbody.AddForce (Vector2.up * jumpspeed, ForceMode2D.Impulse);
            isMoving = true;
            anim.SetBool("Jump", true);
		}

        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.W)) {
            isMoving = false;
            anim.SetBool("isWalking", false);
            anim.SetBool("Jump", false);
        }

        if (isMoving == true)
        {
            footsteps.UnPause();
        }
        else
        {
            footsteps.Pause();
        }

        if (isGrounded == false)
        {
            jump.UnPause();
        }
        else
        {
            jump.Pause();
        }
    }

	void Facing () {
		if (facingLeft == false) {
			transform.localRotation = Quaternion.Euler(0, 0, 0);
		} else {
			transform.localRotation = Quaternion.Euler(0, 180, 0);
		}
	}
}
