﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public float playerHealth = 500f;
    bool isDead = false;
    public Slider healthSlider;

    float currectHealth;

	// Use this for initialization
	void Start () {
        currectHealth = playerHealth;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Damage(float damage) {
        currectHealth -= damage;
        healthSlider.value = currectHealth;
        if (currectHealth <= 0) {
            isDead = true;
            Death();
        }
    }

    public void Death() {
        Destroy(gameObject);
    }
}
