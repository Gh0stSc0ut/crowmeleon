﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    public float speed;
    bool facingRight = false;
    Animator anim;
    EnemyHealth_Level enemyHealth;
    Rigidbody2D rigidbody;
    public GameObject graphic;
    float flipChance;
    public float flipTime;
    bool canFlip;

    // Use this for initialization
    void Start () {
        enemyHealth = GetComponent<EnemyHealth_Level>();
        speed = enemyHealth.enemeySpeed;
        rigidbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time > flipChance) {
            if (Random.Range(0, 10) >= 5) {
                Facing();
            }
            flipChance = Time.time + flipTime;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            if (facingRight && collision.transform.position.x < transform.position.x) {
                Facing();
            } else if (facingRight && collision.transform.position.x > transform.position.x)
            {
                Facing();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            canFlip = false;
            if (!facingRight)
            {
                rigidbody.AddForce(new Vector2(-1, 0) * speed);
            }
            else if (facingRight) {
                rigidbody.AddForce(new Vector2(1, 0) * speed);
            }
            anim.SetBool("Run", true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            canFlip = true;
            rigidbody.velocity = new Vector2(0,0);
            anim.SetBool("Run", false);
        }
    }

    void Facing() {
        if (!canFlip) return;
        float facingX = graphic.transform.localScale.x;
        facingX *= -1f;
        graphic.transform.localScale = new Vector3(facingX, graphic.transform.localScale.x, graphic.transform.localScale.z);
        facingRight = !facingRight;
    }
}
